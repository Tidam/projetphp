<?php
$base = 'sqlite:/tmp/films.sqlite3';
try {
  $file_bd = new PDO($base);
} catch (SQLiteException $e) {
  die("La création ou l'ouverture de la base [$base] a échouée ".
      "pour la raison suivante: ".$e->getMessage());
}
// Ajout de données dans la table
$stmt=$file_bd->prepare("INSERT INTO films (TITRE, REALISATEUR, DATER, GENRE) VALUES (:TITRE, :REALISATEUR , :DATER, :GENRE)");
$result = $stmt->execute(array(
    'TITRE' => $_POST['Titre'],
    'REALISATEUR' => $_POST['Realisateur'],
    'DATER' =>  strtotime($_POST['DateR']),
    'GENRE' => $_POST['Genre']
));
if ($result === FALSE) {
  echo "La requête a échouée";
}# else {
#  echo "Un nouvel enregistrement a été ajouté à la base";
#}
header('Location: http://localhost:8000/index.php');
exit();
// Deconnexion
$bd = null;
?>


    