<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Les Films</title>
    <link rel="stylesheet" href="couleur.css">
    <link rel="stylesheet" href="mise_en_forme.css">
</head>

<body>
<header>
    <div class="drap">
    </div>
    <div class="Titre">
        <h1>Vos liste de films</h1>
        <h1>parce que vous le méritez</h1>
    </div>
    <nav>
        <ul>
            <li><a href="index.php">Accueil</a></li>
        </ul>
    </nav>
</header>
<main>
    <?php
    if (!isSet($_COOKIE['compteur'])){
        $val=$_COOKIE['compteur']+1;
        echo $val;
        try{

            $file_db=new PDO('sqlite:/tmp/films.sqlite3');
            $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
            $file_db->exec("CREATE TABLE IF NOT EXISTS films ( 
          ID INTEGER PRIMARY KEY,
          TITRE TEXT,
          REALISATEUR TEXT,
          DATER INTEGER,
          GENRE TEXT)");

            $films=array(
                array('TITRE' => 'Ratatouille',
                    'REALISATEUR' => 'Brad Bird',
                    'DATER' =>  strtotime('2007'),
                    'GENRE' => 'Comédie'),
                array('TITRE' => 'Ratatouille2',
                    'REALISATEUR' => 'Brad Bird',
                    'DATER' =>  strtotime('2007'),
                    'GENRE' => 'Tragédie'),
                array('TITRE' => 'Ratatouille3',
                    'REALISATEUR' => 'Brad Bird',
                    'DATER' =>  strtotime('2007'),
                    'GENRE' => 'Comique')
            );

            $insert="INSERT INTO films (TITRE, REALISATEUR, DATER, GENRE) VALUES (:TITRE, :REALISATEUR , :DATER, :GENRE)";
            $stmt=$file_db->prepare($insert);
            // on lie les parametres aux variables
            $stmt->bindParam(':TITRE',$titre);
            $stmt->bindParam(':REALISATEUR',$realisateur);
            $stmt->bindParam(':DATER',$dateR);
            $stmt->bindParam(':GENRE',$genre);

            foreach ($films as $f){
                $titre=$f['TITRE'];
                $realisateur=$f['REALISATEUR'];
                $dateR=$f['DATER'];
                $genre=$f['GENRE'];
                $stmt->execute();
            }

            echo "Insertion en base reussie !";
            // on va tester le contenu de la table contacts
            // on ferme la connexion
            //$file_db=null;
        }
        catch(PDOException $ex){
            echo $ex->getMessage();
        }
    }
    else{
    ?>
    <table>
        <thread>
            <tr>
                <th>TITRE</th>
                <th>REALISATEUR</th>
                <th>DATER</th>
                <th>GENRE</th>
            </tr>
        </thread>
        <tbody>
        <?php
        $file_db=new PDO('sqlite:/tmp/films.sqlite3');
        $result=$file_db->query('SELECT * from films');
        foreach ($result as $m){
            echo "<tr>";
            echo "<td>".$m['TITRE']."</td>";
            echo "<td>".$m['REALISATEUR']."</td>";
            echo "<td>".date('Y',$m['DATER'])."</td>";
            echo "<td>".$m['GENRE']."</td>";
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>
    <form action="recherche.php" method="POST">
        <select name="ID">
            <?php
            foreach ($file_db->query('SELECT * from films') as $row)
                if(!empty($row['GENRE']))
                    echo "<option value='".$row['GENRE']."'>"
                        .$row['GENRE']."</option>\n";
            ?>
        </select>
        <select name="ID2">
            <?php
            foreach ($file_db->query('SELECT * from films') as $row)
                if(!empty($row['DATER']))
                    echo "<option value='".strtotime($row['DATER'])."'>"
                        .$row['DATER']."</option>\n";
            ?>
        </select>
        <select name="ID3">
            <?php
            foreach ($file_db->query('SELECT * from films') as $row)
                if(!empty($row['REALISATEUR']))
                    echo "<option value='".$row['REALISATEUR']."'>"
                        .$row['REALISATEUR']."</option>\n";
            ?>
        </select>
        <input type="submit" value="Rechercher">
    </form>
    <form action="add.php" method="POST">
        Titre: <input type="text" name="Titre" required>
        Realisateur: <input type="text" name="Realisateur" required>
        DATER: <input type="date" name="DateR" required>
        <label>Genre:</label>
        <select name="Genre">
            <option value="Comédie">Comédie</option>
            <option value="Tragédie">Tragédie</option>
            <option value="Action">Action</option>
            <option value="Aventure">Aventure</option>
            <option value="Comique">Comique</option>
            <option value="Documentaire">Documentaire</option>
            <option value="Fantastique">Fantastique</option>
        </select>
        <input type="submit">
    </form>
    <form action="del.php" method="POST">
        <select name="Titre">
            <?php
            foreach ($file_db->query('SELECT * from films') as $row)
                if(!empty($row['TITRE']))
                    echo "<option value='".$row['TITRE']."'>"
                        .$row['TITRE']."</option>\n";
            ?>
        </select>
        <input type="submit" value="Supprimer">
    </form>
</main>
<?php
$val= 1;
}
setCookie("compteur",$val);
?>
<footer>
    <h4>Découvre ta liste de films préféré!</h4>
    <nav>
        <a href="index.php">Accueil</a>
        <a href="">Plan du site</a>
        <a href="">Mentions légales</a>
        <a href="">Tous droits réservés Rian Compagny</a>
        <a href="">Contact</a>
    </nav>
</footer>
</body>

</html>